import React, { useEffect, useState } from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Navbar from './components/layouts/Navbar.js';
import Catalog from './components/Catalog.js';
import Cart from './components/forms/Cart.js';
import Transactions from './components/Transactions.js';


// Forms
import AddProductForm from './components/forms/AddProductForm.js';
import LoginForm from './components/forms/LoginForm.js';
import RegisterForm from './components/forms/RegisterForm.js';

import AdminPanel from './components/AdminPanel.js'

function App() {

  const [products, setProducts] = useState([{
        name: null,
        categoryId: null,
        price: null,
        description: null,
        image: null
    }])

  const [categories, setCategories] = useState([]);

  const [cart, setCart] = useState([]);

  const [total, setTotal] = useState(0);

  useEffect(() => {
      let total = 0;
      cart.forEach( item => {
        total += item.price;
      })
      console.log(total);
      setTotal(total);
  }, [cart])


  useEffect(() => {
     fetch("http://localhost:3001/products", {
        method: "GET"
      }) /*pag ala ng 2nd parameter si fetch, means method GET sya*/
     .then(data => data.json())
      .then(product => {setProducts(product)
        })

      //categories
      fetch("http://localhost:3001/categories", {
        method: "GET"
      }) /*pag ala ng 2nd parameter si fetch, means method GET sya*/
     .then(data => data.json())
      .then(categories => {setCategories(categories)
        })

      },[])



  // useEffect(() => {
  //   console.log('state has been changed')
  //    })
   
   //cart methods

  const handleAddToCart = (product) => {
    let matched = cart.find(item => {
      return item._id === product._id;
    })

    console.log(matched);

    if(!matched) {
    setCart([
        ...cart,
        product
      ])
      
    }
  }

  const handleClearCart = () => {
    setCart([]);
  }

  const handleRemoveItem = (itemToBeRemoved) => {
      let updatedCart = cart.filter(item => {
        return ( item !== itemToBeRemoved)
      })

      setCart(updatedCart);
  }
   
// console.log(products)
  return (
    <Router>
    <div>
     <Navbar/> 
        <Switch> 
          <Route exact path="/">
            <Catalog
            products={products}
            handleAddToCart={handleAddToCart}/> 
          </Route>

            <Route exact path="/add-product">
            <AdminPanel
            categories={categories}
            products={products}
            /> 
          </Route>

            <Route exact path="/register">
            <RegisterForm/> 
          </Route>

             <Route exact path="/login">
            <LoginForm/> 
          </Route>

          <Route exact path="/cart">
            <Cart
                cart={cart}
                handleClearCart={handleClearCart}
                handleRemoveItem={handleRemoveItem}
                total={total}
                /> 
          </Route>

          <Route path="/transactions">
            <Transactions
                
                /> 
          </Route>

        
        </Switch> 
    </div>
    </Router>
  )
}

export default App;