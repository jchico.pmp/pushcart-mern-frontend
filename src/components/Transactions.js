import React, { useState, useEffect } from 'react';
import TransactionDetail from './forms/TransactionDetail.js';




const Transactions = () => {

	const [transactions, setTransactions] = useState([]);

	useEffect( () => {
	let url = 'http://localhost:3001/transactions';

		fetch(url, {
			headers : {
				"Authorization" : localStorage.getItem('token')
			}
			})
		.then(data => data.json())
		.then(transactions => setTransactions(transactions))
		}, [])

	return (
						
						<React.Fragment>
							<div className="container">
									<div className="row">
											<div className="col-md-8 offset-md-2">
															<h2 className="text-center pt-3">Transactions Page</h2>	
															
															{
																
																transactions.map( transaction => {
																	return (
																					<TransactionDetail 
																			transaction={transaction}/>
																				)														
																														})

															}
															
											</div>
									</div>
							</div>
							</React.Fragment>



		)
}


export default Transactions;