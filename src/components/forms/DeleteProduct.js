import React, { useState } from 'react';




const DeleteProduct = ({products}) => {

const [selectedProduct, setSelectedProduct] = useState({

		_id: null
})

const handleChange = (e) => {
		setSelectedProduct({
				_id : e.target.value
		})
}

const handleDeleteProduct = e => {
	e.preventDefault();

	let url = 'http://localhost:3001/products/'+selectedProduct._id;

	fetch(url, {
		method: "DELETE",
		headers: {
			"Authorization": localStorage.getItem('token')
		}
	})
	.then(data => data.json())
	.then(result => console.log(result))
}

	return (
						
						<React.Fragment>
									
									<h2 className="text-center">Delete Product</h2>

									<form onSubmit={handleDeleteProduct}>
									{/*//product name*/}
										<div className="form-group">
										<label htmlFor="productId">Product:</label>
										<select
													name="productId"
													id="product"
													className="form-control"
													onChange={handleChange}
													>
														<option disabled selected>Select Product</option>

													{

														products.map(product => {
															return (
																		<option value={product._id}>{product.name}</option>
																)
														})
													}

											
												


										</select>

										<button type="submit" className="btn btn-danger">Delete Product</button>
										</div>


										</form>
							</React.Fragment>



		);
}


export default DeleteProduct;