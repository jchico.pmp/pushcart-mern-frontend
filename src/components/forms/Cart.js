import React, { useState } from 'react';




const Cart = ({cart, handleClearCart, handleRemoveItem, total}) => {

	const handleCheckout = () => {
		let orders = cart.map( item => {
			return {
					id: item._id,
					qty: 1,

			}
		})

		console.log({orders : orders});

		let url = "http://localhost:3001/transactions";

		fetch(url, {
			method: "POST",
			headers: {
				"Content-Type" : "application/json",
				"Authorization" : localStorage.getItem('token')
			},
			body : JSON.stringify({orders})
		})
		.then(data => data.json())
		.then( result => console.log(result))
	}

	return (
						
						<React.Fragment>
									<div className="container">
										<div className="row">
											<div className="col-md-8 offset-md-2">

											{

												!cart.length ? <h2 className="text-center pt-3">Empty guys</h2> :
												<React.Fragment>
													<h2 className="text-center pt-3">My Cart</h2>
											

											
														<table className="table table-striped">
													  <thead className="thead-dark">
													    <tr>
													      <th scope="col">#</th>
													      <th scope="col">Product Name</th>
													      <th scope="col">Price</th>
													      <th scope="col">Action</th>
													    </tr>
													  </thead>
													  <tbody>
													    {/*// startofrow*/}

													    	{
													    		
													    		cart.map( item => {
													    			
														    		return (
																					<tr>
																      <th scope="row">
																     		 {item.categoryId}
																      </th>
																      <td>
																      		{item.name}
																      </td>
																      <td>
																      		{item.price}
																      </td>
																      <td>
																					<button onClick={ () => {handleRemoveItem(item)} } class="btn btn-danger">Remove From Cart</button>
																      </td>
																    </tr>
	
														    			)
														    	})
													    	}
																		
													   
													    {/*endofrow*/}
													    
													  </tbody>
													  <tfoot>
																<th scope="row" colspan="2" className="text-right">
													     		 Total
													      </th>
													      <td>
													      		{total}
													      </td>
													      <td>
													      		<button onClick={handleCheckout} class="btn btn-success">Checkout</button>
													      </td>
													  </tfoot>
													</table>

													<div>
															<button onClick={handleClearCart} class="btn btn-warning">Clear Cart</button>
													</div>
													</React.Fragment>

											}

											</div>
										</div>
									</div>

									
							</React.Fragment>



		);
}


export default Cart;