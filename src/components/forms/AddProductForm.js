import React, { useState } from 'react';




const AddProductForm = ({categories}) => {

const[product, setProduct] = useState({
				name: null,
				price: null,
				categoryId: null,
				description: null,
				image: null
	});

	const { name, price, category, description, image } = product;

	const handleChange = (e) => {
			setProduct({
				...product,
					[e.target.name] : e.target.value
					
			})
	}

	const handleChangeFile = (e) => {
				console.log(e.target.files[0]);				
				setProduct({
					...product,
					image: e.target.files[0]
				});
	}

	const handleAddProduct = e => {
			e.preventDefault();
			const formData = new FormData();

			formData.append('name', product.name);
			formData.append('price', product.price);
			formData.append('categoryId', product.categoryId);
			formData.append('description', product.description);
			formData.append('image', product.image);


			console.log(formData.get('name'));
			console.log(formData.get('price'));
			console.log(formData.get('categoryId'));
			console.log(formData.get('description'));
			console.log(formData.get('image'));

			let url = 'http://localhost:3001/products/';
			fetch(url, {
					method: "POST",
					body: formData,
					headers: {
						"Authorization": localStorage.getItem('token')
					}
			})
			.then( data => data.json())
			.then( result => console.log(result))
	}

	return (
						
							

						<React.Fragment>
						<h2 className="text-center">Add Product Form</h2>
						{JSON.stringify(product)}

									<form enctype="multipart/form-data" onSubmit={handleAddProduct}>
									{/*//product name*/}
										<div className="form-group">
												<label htmlFor="name">Product Name:</label>
												<input
												type="text"
												className="form-control"
												id="name"
												name="name"
												value={name}
												onChange={handleChange}
																/>
										</div>

										{/*product category*/}
										<div className="form-group">
										<label htmlFor="category">Product Category:</label>
										<select
													name="categoryId"
													id="categoryId"
													className="form-control"
													onChange={handleChange}
													>
													<option selected disabled value="">Choose Category</option>

												{
														categories.map( (category) => {
															return (
																	<option value={category._id}>{category.name}</option>
																)
														})

												}

												


										</select>
										</div>

										{/*//product price*/}
										<div className="form-group">
												<label htmlFor="price">Product Price:</label>
												<input
												type="number"
												className="form-control"
												id="price"
												name="price"
												value={price}
												min="1"
												onChange={handleChange}
																/>
										</div>

										{/*//product description*/}
										<div className="form-group">
												<label htmlFor="description">Product Description:</label>
												<input
												type="text"
												className="form-control"
												id="description"
												name="description"
												value={description}
												onChange={handleChange}
																/>
										</div>

										{/*//product image*/}
										<div className="form-group">
												<label htmlFor="image">Product Image:</label>
												<input
												type="file"
												className="form-control-file"
												id="image"
												name="image"
												
												onChange={handleChangeFile}
																/>
										</div>

										<button type="submit" className="btn btn-success">Add Product</button>


									</form>

						</React.Fragment>


		);
}


export default AddProductForm;