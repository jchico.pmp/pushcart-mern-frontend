import React, { useState } from 'react';




const AddCategory = () => {
		

		const[formData, setFormData] = useState({
				
	});

	const { firstname, lastname, email, password, confirmPassword } = formData;

	const handleCategoryNameChange = (e) => {
		setFormData(e.target.value);
	}

	const handleAddCategory = (e) => {
		e.preventDefault();

		let url = "http://localhost:3001/categories";
		let data = { name : formData};
		let token = "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVlM2EyNGUzMDBiOWQ2MWEzZTk0ZjZlNSIsImlhdCI6MTU4MDg2ODkwMH0.NcqdV4Eu0Fhd3b8yUZIL2bHTB6KW4ClNN8YSPIm4Xx4"
		
		fetch(url, {
			method : "POST",
			body: JSON.stringify(data),
			headers: {
				"Content-Type" : "application/json",
				"Authorization" : token
			},

		})
		.then( data => data.json())
		.then( category => console.log(category))
	}

	return (
						
						<React.Fragment>
									
									<h2 className="text-center">Add Category Form</h2>

									<form onSubmit={handleAddCategory}>
									{/*//product name*/}
										<div className="form-group">
												<label htmlFor="name">Category Name:</label>
												<input
												type="text"
												className="form-control"
												id="name"
												name="name"
												
												onChange={handleCategoryNameChange}
																/>
										</div>

										<button type="submit" className="btn btn-primary">Add Category</button>
										</form>
							</React.Fragment>



		);
}


export default AddCategory;