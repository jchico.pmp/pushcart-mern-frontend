import React, { useState, useEffect } from 'react';




const TransactionDetail = ({transaction}) => {

	
	return (
						
						<React.Fragment>
							
															

															<table className="table table-striped">
																<tr>
																		<th>User</th>
																		<td>{transaction.userId}</td>
																</tr>
																<tr>
																		<th>Transaction Code</th>
																		<td>{transaction.transactionCode}</td>
																</tr>
																<tr>
																		<th>Status</th>
																		<td>{transaction.status}</td>
																</tr>
																<tr>
																		<th>Payment Mode</th>
																		<td>{transaction.paymentMode}</td>
																</tr>
																<tr>
																		<th>Date</th>
																		<td>{transaction.dateCreated}</td>
																</tr>
															</table>

															<table className="table table-striped">
																	<thead className="thead-dark">
																			<td>Product Name</td>
																			<td>Quantity</td>
																			<td>Price</td>
																			<td>Subtotal</td>
																	</thead>
																	<tbody>

																	{
																		
																		transaction.products.map( product => {
																			
																			return (
																							<tr>
																								<td>{product.name}</td>
																								<td>{product.quantity}</td>
																								<td>{product.subtotal}</td>
																								<td>{product.subtotal}</td>
																							</tr>
																							
																					)
																		})
																	}
																			

																	</tbody>
																	<tfoot>
																<th scope="row" colspan="3" className="text-right">
													     		 Total
													      </th>
													      <td>
													      		{transaction.total}
													      </td>
													      
													  </tfoot>
															</table>
											
							</React.Fragment>



		)
}


export default TransactionDetail;