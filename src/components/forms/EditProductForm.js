import React, { useState } from 'react';




const EditProductForm = ({categories, products}) => {

	 const [selectedProduct, setSelectedProduct] = useState({
					id: null,
					name: null,
					image: null,
					price: null,
					description: null,
					categoryId: null
	
	 				})

	 const handleSelectedProduct = (e) => {
				let selected = products.find(product => {
					return (product._id == e.target.value)
				})

				let currCategory = categories.find(category => {
					return category._id === selected.categoryId
				})

				setSelectedProduct({
					...selected,
					image: "http://localhost:3001"+selected.image
				});
	 }

	 const handleChange = e => {
	 	setSelectedProduct({
	 		...selectedProduct,
	 		[e.target.name] : e.target.value
	 	})
	 }

	 const handleChangeFile = (e) => {
				setSelectedProduct({
					...selectedProduct,
					image: e.target.files[0]
				});
				console.log(selectedProduct);
				console.log(typeof e.target.files[0]);				
	}


	const handleEditProduct = (e) => {
		e.preventDefault();
		// console.log(selectedProduct);

		let formData = new FormData();

		formData.append('name', selectedProduct.name);
		formData.append('price', selectedProduct.price);
		formData.append('categoryId', selectedProduct.categoryId);
		formData.append('description', selectedProduct.description);

		if(typeof selectedProduct.image === 'object') {
		formData.append('image', selectedProduct.image);
			
		}
		 console.log(formData.get('image'));

		let url = "http://localhost:3001/products/"+selectedProduct._id;

		fetch(url, {
			method : "PUT",
			headers : {
				"Authorization" : localStorage.getItem('token')
			},
			body: formData
		})
		.then( data => data.json())
		.then( result => console.log(result))
	}


	return (
						
							<React.Fragment>
									<h2 className="text-center">Edit Product Form</h2>
								
								<label htmlFor="product">Product :</label>
								<select name="product" id="product" className="form form-control" onChange={handleSelectedProduct}>
								<option disabled selected>Select to Product to Edit</option>
									{
										products.map( product => {
											return (
														<option value={product._id}>{product.name}</option>
												)

											
										})
									}
								</select>
										
										<div className="row">
											<div className="col-4">
												<img width="50px" src={selectedProduct.image} alt=""/>
											</div>
											<div className="col-8">
												<p className="mb-0">Name: {selectedProduct.name}</p>
												<p className="mb-0">Price: {selectedProduct.price}</p>
												<p className="mb-0">Category: {selectedProduct.categoryId}</p>
												<p className="mb-0">Description: {selectedProduct.description}</p>
											

											</div>
										</div>


									<form onSubmit={handleEditProduct} enctype="multipart/form-data">
									{/*//product name*/}
										<div className="form-group">
												<label htmlFor="name">Product Name:</label>
												<input
												type="text"
												className="form-control"
												id="name"
												name="name"
												value={selectedProduct.name}
												onChange={handleChange}
																/>
										</div>

										{/*product category*/}
										<div className="form-group">
										<label htmlFor="category">Product Category:</label>
										<select
													name="categoryId"
													id="category"
													className="form-control"
													onChange={handleChange}
													>

												{
													categories.map(category => {
														return (
																category._id === selectedProduct.categoryId ?
																		<option value={category._id} selected>{category.name}</option> :
																		<option value={category._id}>{category.name}</option>
															)
													})
												}


										</select>
										</div>

										{/*//product price*/}
										<div className="form-group">
												<label htmlFor="price">Product Price:</label>
												<input
												type="number"
												className="form-control"
												id="price"
												name="price"
												value={selectedProduct.price}
												min="1"
												onChange={handleChange}
																/>
										</div>

										{/*//product description*/}
										<div className="form-group">
												<label htmlFor="description">Product Description:</label>
												<input
												type="text"
												className="form-control"
												id="description"
												name="description"
												value={selectedProduct.description}
												onChange=""
																/>
										</div>

										{/*//product image*/}
										<div className="form-group">
												<label htmlFor="image">Product Image:</label>
												<input
												type="file"
												className="form-control"
												id="image"
												name="image"
												
												onChange={handleChangeFile}
																/>
										</div>

										<button type="submit" className="btn btn-warning">Edit Product</button>


									</form>
							</React.Fragment>
	


		);
}


export default EditProductForm;