import React, { useState } from 'react'

const LoginForm = () => {

	const [formData, setFormData] = useState({
		email : null,
		password : null
	})

	const [result, setResult] = useState({
		message : null,
		successful : null
	})

	const handleChange = e => {
		setFormData({
			...formData,
			[e.target.name] : e.target.value
		})
	}

	const handleLogin = e => {
		e.preventDefault();
		let data = {
			email : formData.email,
			password : formData.password,
		}

		console.log(data)
		fetch('http://localhost:3001/users/login', {
			method : "POST",
			body : JSON.stringify(data),
			headers : {
				"Content-Type" : "application/json"
			}
		})
		.then( data => data.json() )
		.then( user => {
			console.log('user')

			if (user.token) {
				setResult({
					successful : true,
					message : user.message
				})
				localStorage.setItem('user', JSON.stringify(user.user));
				localStorage.setItem('token', "Bearer " + user.token);
			} else {
				setResult({
					successful : false,
					message : user.message	
				})
			}
		} )
	}

	const resultMessage = () => {
		let classList;
		if (result.successful === true) {
			classList = 'alert alert-success'
		} else {
			classList = 'alert alert-warning'
		}

		return (
			<div className={classList} >
				{result.message}
			</div>
		)
	}

	return(
		<div className='container'>
			<div className='row'>		
				<div className='col-12 col-md-6 mx-auto'>
					<form action="" onSubmit={handleLogin}>
					<h2 className="text-center">Login</h2>
					{result.successful == null ? "" : resultMessage()}
						<div class="form-group">
							<label for="email">Email address</label>
							<input type="email" class="form-control" id="email" name="email" aria-describedby="emailHelp" onChange={handleChange}></input>
						</div>
						<div class="form-group">
							<label for="password">Password</label>
							<input type="password" class="form-control" id="password" name="password" onChange={handleChange}></input>
						</div>
						<button type="submit" class="btn btn-primary">Submit</button>
					</form>
				</div>
			</div>
		</div>
	)
}

export default LoginForm