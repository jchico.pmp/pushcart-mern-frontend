import React, { useState } from 'react';


const RegisterForm = () => {

	const[formData, setFormData] = useState({
				firstname : "",
				lastname : "",
				email : "",
				password : "",
				confirmPassword : ""


	});

	const { firstname, lastname, email, password, confirmPassword } = formData;

	const onChangeHandler = (e) => {
		setFormData({
			...formData,
			[e.target.name]: e.target.value
		})
	};

	const handleRegister = e => {
		//para di magreload ang page
		e.preventDefault();

		if(password !== confirmPassword) {
			alert("Passwords do not match");
		} else {
			fetch("http://localhost:3001/users/register", {
				method: "POST",
				headers: {
					"Content-Type" : "application/json"
				},
				body : JSON.stringify(formData)
			})
			.then( data => {
				return data.json()})
			.then( user => {
				console.log(user)
				setFormData({
					firstname : "",
				lastname : "",
				email : "",
				password : "",
				confirmPassword : ""

				})

					if(user.message) {
						let element = 
						document.getElementById("message");
						element.innerHTML = user.message;
						element.classList.toggle("d-none");
						setTimeout( function() {
							element.classList.toggle("d-none")
						}, 3000)

					}

					if(user.successMessage) {
						let elementS = 
						document.getElementById("smessage");
						let element = 
						document.getElementById("message");
						elementS.innerHTML = user.successMessage;
						element.classList.toggle("d-none");
						elementS.classList.toggle("d-none");
						setTimeout( function() {
							elementS.classList.toggle("d-none")
						}, 3000)
						setTimeout( function() {
							element.classList.toggle("d-none")
						}, 3000)

					}


			})
		}
	}

	console.log(formData);


	return (
						<div className="container">
						{JSON.stringify(formData)}
						<div className="col-lg-6 mx-auto pt-4">

						<div className="alert alert-danger" role="alert" id="message">Incomplete fields</div>

						<div className="alert alert-success d-none" role="alert" id="smessage">You are now registered.</div>



							<h2>Registration Page</h2>

							<form onSubmit={e => handleRegister(e)}>
								{/*//firstname*/}
								<div className="form-group">
										<label htmlFor="firstname">First Name:</label>
										<input
										type="text" 
										className="form-control"
										id="firstname"
										name="firstname"
										value={formData.firstname}    
										onChange={onChangeHandler}
										/>
								</div>

								{/*//lastname*/}
								<div className="form-group">
										<label htmlFor="firstname">Last Name:</label>
										<input
										type="text"
										className="form-control"
										id="lastname"
										name="lastname"
										value={lastname}    
										onChange={onChangeHandler}
										/>
								</div>
								{/*//email*/}
								<div className="form-group">
										<label htmlFor="email">Email:</label>
										<input
										type="email"
										className="form-control"
										id="email"
										name="email"
										value={email}    
										onChange={onChangeHandler}
										/>
								</div>
								{/*//password*/}
								<div className="form-group">
										<label htmlFor="password">Password:</label>
										<input
										type="password"
										className="form-control"
										id="password"
										name="password"
										value={password}    
										onChange={onChangeHandler}

										/>
								</div>
								{/*//confirm password*/}
								<div className="form-group">
										<label htmlFor="confirmPassword">Confirm Password:</label>
										<input
										type="password"
										className="form-control"
										id="confirmPassword"
										name="confirmPassword"
										value={confirmPassword}    
										onChange={onChangeHandler}

										/>
								</div>

								<button
								type="submit"
								className="btn btn-primary"
								>Submit</button>

							</form>

						</div>
							
						</div>

		);
}


export default RegisterForm;