import React from 'react';
import AddProductForm from './forms/AddProductForm.js';
import EditProductForm from './forms/EditProductForm.js'
import AddCategory from './forms/AddCategory.js';
import EditCategory from './forms/EditCategory.js'
import DeleteProduct from './forms/DeleteProduct.js'
import DeleteCategory from './forms/DeleteCategory.js';




const AdminPanel = ({categories, products}) => {


	return (
						
						<div className="container">
							<div className="row">
								<div className="col-lg-6 mx-auto pt-4">

											<AddProductForm categories={categories}/>

								</div>

								<div className="col-lg-6 mx-auto pt-4">

											<AddCategory/>
											<EditProductForm
											categories={categories}
											products={products}
											/>

								</div>

							

							</div>

							<div className="row">
							<div className="col-lg-6 mx-auto pt-4">
										
										<DeleteCategory/>
										<DeleteProduct products={products}/>

							</div>
							<div className="col-lg-6 mx-auto pt-4">
									<EditCategory/>
							</div>
							</div>
						</div>

						
							

						


		);
}


export default AdminPanel;